# Multi-threaded Sorting Algorithms

This repository contains implementations of merge sort in both C++ and C#. The primary focus of these implementations is to explore the performance differences between single-threaded (synchronous) and multi-threaded (asynchronous) approaches.

## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Usage](#usage)
- [Performance Comparisons](#performance-comparisons)

## Features

1. **C++ Implementation**:
    - Utilizes the C++ Standard Library.
    - Uses multi-threading to split array segments and process them in parallel.
    - Dynamically identifies the number of available CPU cores.

2. **C# Implementation**:
    - Exploits the power of C# Task Parallel Library.
    - Contains both synchronous (`SyncSort`) and asynchronous (`AsyncSort`) versions.
    - User-friendly console interface for inputting and displaying sorted arrays.

## Requirements

1. **For C++**:
    - A C++ compiler supporting the C++11 standard or later.
    - Windows OS (because of the use of `windows.h`).

2. **For C#**:
    - .NET SDK (Core or Framework).
    - A suitable IDE like Visual Studio or VS Code for a better experience.

## Usage

### C++:

Compile and run the C++ file:

```bash
g++ merge_sort.cpp -o merge_sort -std=c++11 -pthread
./merge_sort
```

### C#:

Navigate to the project directory and run:

```bash
dotnet run
```

Or, if using Visual Studio, open the solution and press F5 to run.

## Performance Comparisons

The console output for both implementations will provide the time taken for sorting using the synchronous and asynchronous methods. It's interesting to note the performance gains, especially for large arrays, when using multi-threading.

