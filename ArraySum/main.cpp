#include <iostream>
#include <windows.h>
#include <thread>
#include <vector>
#include <ctime>

using namespace std;

// Function to compute the sum for a given segment of the array in a separate thread
void threadFunction(int threadnumber, int single_core_array_length, int array_length, int cpu_cores, 
                    int single_core_array_length_remainder, int arr[], long long sumarr[]) 
{
    long long localsum = 0;

    // Check if it's the last thread to add remainder to its segment 
    int local_single_core_array_length_remainder = (threadnumber == (cpu_cores - 1)) ? single_core_array_length_remainder : 0;

    for (int i = threadnumber * single_core_array_length; 
         i < (threadnumber + 1) * single_core_array_length + local_single_core_array_length_remainder; i++)
    {
        localsum += arr[i];
    }

    cout << "Thread " << threadnumber << " localsum " << localsum << endl;  
    sumarr[threadnumber] = localsum;  
}

int main() 
{
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    int cpu_cores = sysinfo.dwNumberOfProcessors;  // Get the number of CPU cores

    cout << "Number of CPUs: " << cpu_cores << endl;

    const int array_length = 100000000;  
    cpu_cores = 16;  // Manually set the number of threads (but not more than 256)

    cout << "Number of CPUs in use: " << cpu_cores << endl;

    int single_core_array_length = array_length / cpu_cores; 
    int single_core_array_length_remainder = array_length % cpu_cores;

    cout << "Single core array length: " << single_core_array_length << endl;
    cout << "Single core array length remainder: " << single_core_array_length_remainder << endl;

    int* arr = new int[array_length];  // Dynamic array for random integers
    long long sumarr[256] = {0};       // Static array to hold the sums from each thread

    long long sum = 0;
    for (int i = 0; i < array_length; i++) 
    {
        arr[i] = rand() % 100;         // Populate the array with random values between 0 to 99
        sum += arr[i];
    }

    float start_time = clock();  // Start the timer

    vector<thread> threads;
    for (int i = 0; i < cpu_cores; i++) 
    {
        threads.emplace_back(threadFunction, i, single_core_array_length, array_length, cpu_cores, single_core_array_length_remainder, arr, sumarr);
    }

    for (auto& th : threads) 
    {
        th.join();
    }

    float end_time = clock();    // End the timer

    long long sum_from_threads = 0;
    for (int i = 0; i < cpu_cores; i++) 
    {
        sum_from_threads += sumarr[i];
        cout << sumarr[i] << " ";
    }

    cout << endl << (end_time - start_time); 
    cout << "\nArray sum: " << sum; 
    cout << "\nArray sum from threads: " << sum_from_threads;

    cout << "\nEND";

    delete[] arr;  
    return 0;
}
