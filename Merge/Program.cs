using System;
using System.Diagnostics;

namespace sort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Merge Sort");
            Console.Write("Enter array elements (separated by space, comma, or semicolon): ");

            Stopwatch stopwatch = new Stopwatch();
            string[] inputStrings = Console.ReadLine()
                                           .Split(new[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            int[] array = Array.ConvertAll(inputStrings, int.Parse);

            stopwatch.Start();
            int[] sortedSyncArray = SyncSort.MergeSort(array);
            stopwatch.Stop();
            double syncTime = stopwatch.ElapsedMilliseconds;

            Console.WriteLine($"Ordered array (synchronous sort): {string.Join(", ", sortedSyncArray)}");

            stopwatch.Restart();
            int[] sortedAsyncArray = AsyncSort.MergeSortAsync(array);
            stopwatch.Stop();
            double asyncTime = stopwatch.ElapsedMilliseconds;

            Console.WriteLine($"Ordered array (parallel sort): {string.Join(", ", sortedAsyncArray)}");
            Console.WriteLine($"Synchronous execution time: {syncTime} milliseconds");
            Console.WriteLine($"Parallel execution time: {asyncTime} milliseconds");

            Console.ReadLine();
        }
    }
}
