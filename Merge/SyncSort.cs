using System;
using System.Linq;

namespace sort
{
    /// <summary>
    /// Provides synchronous sort methods.
    /// </summary>
    /// <typeparam name="TComparable">The type of elements in the arrays.</typeparam>
    class SyncSort
    {
        /// <summary>
        /// Merges two sorted arrays into a single sorted array.
        /// </summary>
        /// <param name="firstArray">First sorted array.</param>
        /// <param name="secondArray">Second sorted array.</param>
        /// <returns>A merged sorted array.</returns>
        public static TComparable[] Merge<TComparable>(TComparable[] firstArray, TComparable[] secondArray) where TComparable : IComparable
        {
            int firstIndex = 0;
            int secondIndex = 0;
            int resultIndex = 0;
            TComparable[] result = new TComparable[firstArray.Length + secondArray.Length];

            // Merge arrays until one of them runs out
            while (firstIndex < firstArray.Length && secondIndex < secondArray.Length)
            {
                if (firstArray[firstIndex].CompareTo(secondArray[secondIndex]) < 0)
                {
                    result[resultIndex++] = firstArray[firstIndex++];
                }
                else
                {
                    result[resultIndex++] = secondArray[secondIndex++];
                }
            }

            // Copy any remaining elements from the first array
            while (firstIndex < firstArray.Length)
            {
                result[resultIndex++] = firstArray[firstIndex++];
            }

            // Copy any remaining elements from the second array
            while (secondIndex < secondArray.Length)
            {
                result[resultIndex++] = secondArray[secondIndex++];
            }

            return result;
        }

        /// <summary>
        /// Sorts the specified array using the MergeSort algorithm.
        /// </summary>
        /// <param name="array">Array to be sorted.</param>
        /// <returns>A sorted array.</returns>
        public static TComparable[] MergeSort<TComparable>(TComparable[] array) where TComparable : IComparable
        {
            if (array.Length <= 1)
            {
                return array;
            }

            int middleIndex = array.Length / 2;

            // Recursively sort both halves
            TComparable[] leftArray = MergeSort(array.Take(middleIndex).ToArray());
            TComparable[] rightArray = MergeSort(array.Skip(middleIndex).ToArray());

            // Merge the sorted halves
            return Merge(leftArray, rightArray);
        }
    }
}
