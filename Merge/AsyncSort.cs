using System;
using System.Linq;
using System.Threading.Tasks;

namespace sort
{
    class AsyncSort
    {
        /// <summary>
        /// Asynchronously sorts an array using the MergeSort algorithm.
        /// </summary>
        /// <typeparam name="TComparable">Type of the elements in the array.</typeparam>
        /// <param name="array">Array to be sorted.</param>
        /// <returns>Task that represents the asynchronous operation. The value of the TResult parameter contains the sorted array.</returns>
        public static Task<TComparable[]> MergeSort<TComparable>(TComparable[] array) where TComparable : IComparable
        {
            if (array.Length <= 1)
            {
                return Task.FromResult(array);
            }

            int middleIndex = array.Length / 2;

            // Recursively split and sort both halves
            Task<TComparable[]> firstHalf = MergeSort(array.Take(middleIndex).ToArray());
            Task<TComparable[]> secondHalf = MergeSort(array.Skip(middleIndex).ToArray());

            // Wait for both halves to be sorted
            Task.WaitAll(firstHalf, secondHalf);

            // Merge the sorted halves
            return Task.FromResult(SyncSort.Merge(firstHalf.Result, secondHalf.Result));
        }

        /// <summary>
        /// Synchronously sorts an array using the async MergeSort algorithm.
        /// </summary>
        /// <typeparam name="TComparable">Type of the elements in the array.</typeparam>
        /// <param name="array">Array to be sorted.</param>
        /// <returns>Sorted array.</returns>
        public static TComparable[] MergeSortAsync<TComparable>(TComparable[] array) where TComparable : IComparable
        {
            return MergeSort(array).Result;
        }
    }
}
